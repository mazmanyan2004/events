class SectionCreator {
    create(type) {
        let pageType=type;
        document.addEventListener("DOMContentLoaded", function (pageType) {
            
            let section = document.createElement("section");
            section.classList = "app-section app-section--image-culture";
            section.style.backgroundImage = 'url("assets/images/background3.png")';
            section.style.height = "436px";
            section.id="addedSection";

            let header = document.createElement("h2");
            header.className = "app-title";

            let text = document.createElement("span");
            text.className = "app-subtitle";
            text.innerText = "Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.";

            let form = document.createElement("form");
            form.style.width = "60%";
            form.style.display = "flex";
            form.style.justifyContent = "space-between";
            form.style.marginTop = "4%";
            form.style.alignItems = "center";
                        
            let input = document.createElement("input");
            input.type = "text";
            input.placeholder = "Email";
            input.id = "input";
            input.style.background = "rgba(255,255,255,0.3)";
            input.style.border = "none";
            input.style.height = " 30px";
            input.style.width = "70%";
            input.style.padding = "0 20px";

            let button = document.createElement("button");
            button.classList = "app-section__button app-section__button--read-more";
            button.id = "submit-button";
            button.type = "submit";
            button.style.margin = "0";

            let removeButton =  document.createElement("button");
            removeButton.innerHTML="remove";
            button.id = "remove-button";
            removeButton.classList = "app-section__button app-section__button--read-more";

            if (type == 'standard') {

                    header.innerText = "Join Our Program";

                    button.innerText = "SUBSCRIBE";

            }   else if(type ==  'advanced') {

                    header.innerText = "Join Our Advanced Program";

                    button.innerText = "Subscribe to Advanced Program";
                   
            }

            section.appendChild(header);
            section.appendChild(text);
            form.appendChild(input);
            form.appendChild(button);

            section.appendChild(form);
            section.appendChild(removeButton);


            document.getElementById("section1").after(section);

            removeButton.addEventListener('click', function remove (){ 
                if(document.getElementById('addedSection')){
                    let deleteSection = document.getElementById('addedSection');
                    deleteSection.parentNode.removeChild(deleteSection);
                }
            },false)

            form.addEventListener('submit', function (e) {
                e.stopPropagation();
                e.preventDefault();
                console.log(e.target.querySelector('input').value);
            }, false);

         
        }, false);
      

    }

}

export {SectionCreator};

